extends Scrap

func _on_chest_body_entered(body):
	if body.collision_layer == Game.PLAYER_COLLISION_LAYER:
		Game.actual_object = self
		body.enters_object(needed_tool)

func _on_chest_body_exited(body):
	if body.collision_layer == Game.PLAYER_COLLISION_LAYER:
		body.leaves_object()

func play_animation() -> void:
	if !$AnimationPlayer.is_playing() and not object_is_used:
		$AnimationPlayer.play("HatchDrop")
		Game.scrap_amount += 1
		object_is_used = true

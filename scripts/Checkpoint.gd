extends Area2D

onready var Game = get_node("/root/Game")

func _ready():
	$Light2D.enabled = false

func _on_Checkpoint_body_entered(_body):
	$Light2D.enabled = true
	$Timer.start()

func _on_Checkpoint_body_exited(_body):
	$Light2D.enabled = false
	$Timer.stop()


func _on_Timer_timeout():
	Game.power += 25
	Game.power = min(Game.max_power, Game.power)

extends KinematicBody2D

const NEXT_TOOL = 1
const PREV_TOOL = -1

onready var Game = get_node("/root/Game")

export var speed: = Vector2(500, 400)
export var gravity: = 1000.0

var movement: Vector2 = Vector2.ZERO
var is_at_object: = false
var needed_tool = null
var selected_tool = 0

func _process(_delta):
	if Input.is_action_just_pressed("next_tool"):
		switch_tool(NEXT_TOOL)

	elif Input.is_action_just_pressed("prev_tool"):
		switch_tool(PREV_TOOL)
	
	if position.y > 3000:
		Game.game_over = true

func _physics_process(_delta) -> void:
	check_input()
	get_animation()
	movement = calculate_speed()
	movement = move_and_slide(movement, Vector2.UP)

func get_animation() -> void:
	if is_on_floor():
		if Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right"):
			$AnimatedSprite.animation = "Running"
		else:
			$AnimatedSprite.animation = "Idle"
			
	if Input.is_action_pressed("jump") and is_on_floor():
		$AnimatedSprite.animation = "Jumping"

func calculate_speed() -> Vector2:
	movement.y += gravity * get_physics_process_delta_time()
	return Vector2(
		(Input.get_action_strength("move_right") - Input.get_action_strength("move_left")) * speed.x,
		movement.y
	)

func check_input() -> void:
	if Input.is_action_pressed("jump") and is_on_floor():
		movement.y = -speed.y
	
	if Input.is_action_just_released("jump") and movement.y < 0:
		movement.y *= 0.1

	if Input.is_action_just_pressed("use") and is_at_object and needed_tool == selected_tool:
		Game.actual_object.play_animation()
	
	if Input.is_action_pressed("move_left") && !$AnimatedSprite.flip_h:
		$AnimatedSprite.flip_h = true;
		$arms.scale = Vector2(-1, 1);
	
	if Input.is_action_pressed("move_right") && $AnimatedSprite.flip_h:
		$AnimatedSprite.flip_h = false;
		$arms.scale = Vector2(1, 1);
	
	if Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right"):
		if $PowerDrain.is_stopped():
			$PowerDrain.start()
	else:
		$PowerDrain.stop()

func enters_object(object_tool):
	needed_tool = object_tool
	is_at_object = true

func leaves_object():
	is_at_object = false

func update_power():
	Game.power = max(0, Game.power - 1)

func switch_tool(direction: int) -> void:
	selected_tool += direction
	if selected_tool > Game.tool_level:
		selected_tool = 0
	if selected_tool < 0:
		selected_tool = Game.tool_level
	
	for arm in $arms.get_children():
		arm.visible = false
	
	$arms.get_children()[selected_tool].visible = true;
	


extends Area2D

onready var Game = get_node("/root/Game")

export var upgrade_cost = 2

var open_state = preload("res://assets/images/tool_box_open.png")
var close_state = preload("res://assets/images/tool_box_closed.png")
var is_active: = false


func _process(_delta):
	if Input.is_action_just_pressed("use") and is_active:
		if Game.scrap_amount >= upgrade_cost:
			Game.scrap_amount = 0
			Game.tool_level += 1


func _on_UpgradStation_body_entered(_body):
	is_active = true
	$Sprite.texture = open_state


func _on_UpgradStation_body_exited(_body):
	is_active = false
	$Sprite.texture = close_state

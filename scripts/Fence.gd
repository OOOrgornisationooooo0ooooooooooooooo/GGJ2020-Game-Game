extends Scrap

var open_texture: = preload("res://assets/images/Fence_broken.png")

func _on_Fence_body_entered(body):
	if body.collision_layer == Game.PLAYER_COLLISION_LAYER:
		Game.actual_object = self
		body.enters_object(needed_tool)

func _on_Fence_body_exited(body):
	if body.collision_layer == Game.PLAYER_COLLISION_LAYER:
		body.leaves_object()

func play_animation():
		object_is_used = true
		$Sprite.texture = open_texture
		remove_child($DONOTENTER)

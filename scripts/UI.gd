extends Control

onready var Game = get_node("/root/Game")

func _process(_delta):
	$ScrapContainer/ScrapAmount.text = str(Game.scrap_amount)
	$ToolLevelContainer/ToolLevel.text = str(Game.tool_level)
	$EnergyContainer/EnergyNumber.text = str(Game.power)
	$EnergyContainer/EnergyBar.value = Game.power

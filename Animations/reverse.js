#!/usr/bin/node

const fs = require("fs");
var folder = process.argv[2];

fs.readdir(folder, (err, files) => {
  files = files.reverse();
  var i = 0;
  for (f of files) {
    fs.rename(folder + "/" + f, folder + "/" + i + "." + f.split(".")[1], (err) => {
      console.error(err);
    });
    i++;
}
});
